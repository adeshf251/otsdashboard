import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UrlToStringPipe } from './Pipes/url-to-string.pipe';
import { SafeHtmlPipe } from './Pipes/safe-html.pipe';
import { CounterPipe } from './Pipes/counter.pipe';
import { Loading1Component } from './Shared/loading1/loading1.component';
import { Loading2Component } from './Shared/loading2/loading2.component';
import { BrokenUrlComponent } from './Shared/broken-url/broken-url.component';
import { PageNotFoundComponent } from './Shared/page-not-found/page-not-found.component';
import { MaintenanceComponent } from './Shared/maintenance/maintenance.component';
import { SidenavComponent } from './Shared/sidenav/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    UrlToStringPipe,
    SafeHtmlPipe,
    CounterPipe,
    Loading1Component,
    Loading2Component,
    BrokenUrlComponent,
    PageNotFoundComponent,
    MaintenanceComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
