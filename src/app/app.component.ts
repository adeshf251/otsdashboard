import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'otsdashboard';
  usr = localStorage.getItem('connect.sid');

  constructor() {
    console.log(this.usr);

  }
}
