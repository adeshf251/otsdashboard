import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loading1',
  templateUrl: './loading1.component.html',
  styleUrls: ['./loading1.component.css']
})
export class Loading1Component implements OnInit {

  @Input() status: boolean;

  constructor() { }

  ngOnInit() {
  }

}
