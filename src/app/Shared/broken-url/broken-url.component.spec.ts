import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokenUrlComponent } from './broken-url.component';

describe('BrokenUrlComponent', () => {
  let component: BrokenUrlComponent;
  let fixture: ComponentFixture<BrokenUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokenUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokenUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
